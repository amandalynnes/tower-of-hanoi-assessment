        
        https://amandalynnes.gitlab.io/tower-of-hanoi-assessment 

        AJ Rodriguez and Amanda Simmons worked on this project.

         Brandi tutored Amanda on the high level thought process of doing this project.
         Used some code from Randy's demo video.
         Recieved help from Eric Hanson with our eventlisteners and moveDisc function.
         Recieved help from Michelle to debug and problem solve the functionality of our hold and towers.



        step1-figure out a way to pick up and move the top disc

          1.  Create a click handler on towers ---- event.currentTarget
          2. click handler picks up top disc ---- lastElementChild
             a. write function that runs when click event happens
          3. click handler places moves disc 
            a. write function that runs when click event happens
            
        step2-figure out a way to drop/place the discs but make sure it can only take place on top of larger disc
        
            1. determine size of discs with datasets and compare width 
               a. if - if disc being held is smaller than last child in tower
                       then can be placed in tower
               b. else - return 
        
         step3-figure out how to create event for win condition
            1. if third tower is has all four discs - win condition met
               a. count child elements in tower 3
               (tower.childElementCount===4)
               b. alert the player of the win










    Game Rules
    1. Only one disk can be moved at a time.
    2. Each move consists of taking the upper disk from one of the stacks    and placing it on top of another stack.
    3. No disk may be placed on top of a smaller disk.
!!!!---------
You will want to have the player click twice for each move - first to pick the source tower, second to pick the destination tower. Use a variable to keep track of which mode the player is in.

Add a click handler to each of the three towers. Use event.currentTarget inside the event handler to determine which tower was clicked.

Use the DOM property childElementCount to find how many disks are in a tower.

Use the DOM property lastElementChild to find the disk on top of a tower.

Use the DOM method appendChild() to add a disk to a tower (you have already used this method many times in previous assignments). Note that when you use appendChild on an element that already has a parent, it is automatically removed from the old parent and added to the new one.

Use the Element.clientWidth property to get the width of your disk elements.